#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
flooder.py - Phenny Flood Module
Copyright 2014, Akiyama Mio, 0chan.hk
Licensed under the Eiffel Forum License 2.

http://inamidst.com/phenny/
"""

import logging, threading, time, random

messages = [
"na sosach, bydlo",
"ololo nabigaem na irc",
"ALLOU YOBA ETO TY?",
"2ch.hk pwnz",
"Neistovo nullchuyu bump",
"KURWA"
]

# Source must be string, list of strings, or callable which returns string.
def make_string_generator(source):
    if isinstance(source, str):
        return lambda: source
    elif isinstance(source, list):
        return lambda: random.choice(source)
    else:
        return source

class FloodLoop:
    def __init__(self, name, phenny, target, message, period):
        self.logger = phenny.logger.getChild(name)
        self.phenny = phenny
        self.cancelled = False
        self.thread = None
        self.target = make_string_generator(target)
        self.message = make_string_generator(message)

        self.start(period)

    def do_flood(self, period):
        iteration_time = 1.0
        say_period = period / iteration_time
        cycle_counter = 0
        while not self.cancelled:
            self.logger.debug("New iteration of the flood loop.")

            if cycle_counter >= say_period:
                try:
                    self.phenny.msg_nocheck(self.target(), self.message())
                except: pass

                cycle_counter = 0
            else:
                cycle_counter += 1

            time.sleep(iteration_time)

        self.logger.info("Exiting from the flood loop.")

    def start(self, period):
        if self.running():
            raise Exception("The flooder is already running, suka.")

        self.thread = threading.Thread(target = lambda: self.do_flood(period))
        self.cancelled = False
        self.thread.start()
        self.logger.info("Starting new flooder.")

    def running(self):
        return self.thread is not None

    def stop(self):
        self.logger.info("Stopping the flooder.")

        self.cancelled = True
        self.thread.join()
        self.thread = None

import urllib, re

def get_dollar():
    page = urllib.urlopen("http://moex.com/ru/derivatives/currency-rate.aspx").read()
    return "Dollar: " + re.search(r"Текущее значение:  (\d+,\d+)", page).group(1)

def setup(phenny):
    phenny.logger.info("Restarting all the flooders.")

    if phenny.global_state.has_key("flood_workers"):
        for w in phenny.global_state["flood_workers"].keys():
            phenny.logger.info("Stopping flooder '%s'.", w)

            if phenny.global_state["flood_workers"][w].running():
                phenny.logger.info("Flooder '%s' is running.", w)

                phenny.global_state["flood_workers"][w].stop()

    phenny.logger.info("Starting flooders.")

    phenny.global_state["flood_workers"] = {
        "dorrar": FloodLoop("dorrar", phenny, "#0chan", get_dollar, 30.0)
    }

def fstop(phenny, input):
    """.fstop <flooder> - Stop a flooder."""

    if phenny.global_state["flood_workers"].has_key(input.group(2)):
        if phenny.global_state["flood_workers"][input.group(2)].running():
            phenny.global_state["flood_workers"][input.group(2)].stop()

        phenny.reply("Done")
    else:
        phenny.reply("No such flooder")

fstop.commands = ['fstop']

def fstart(phenny, input):
    """.fstart <flooder> <timeout> - Start a flooder."""

    flooder, timeout = input.group(2).split()

    if phenny.global_state["flood_workers"].has_key(flooder):
        if not phenny.global_state["flood_workers"][flooder].running():
            phenny.global_state["flood_workers"][flooder].start(float(timeout))

            phenny.reply("'%s' started" % flooder)
        else:
            phenny.reply("'%s' is already running" % flooder)
    else:
        phenny.reply("No such flooder")

fstart.commands = ['fstart']

def fonce(phenny, input):
    """.fonce <flooder> - Print a message once."""

    if phenny.global_state["flood_workers"].has_key(input.group(2)):
        phenny.say(phenny.global_state["flood_workers"][input.group(2)].message())
    else:
        phenny.reply("No such flooder")

fonce.commands = ['fonce']
