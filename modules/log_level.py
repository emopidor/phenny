#!/usr/bin/env python
# coding=utf-8
"""
log_level.py - setup log level at runtime
Copyright 2014, Akiyama Mio, 0chan.hk
Licensed under the GPLv2.

http://inamidst.com/phenny/
"""

import logging

level = "INFO"

def setup(phenny):
    phenny.logger.setLevel(level)

if __name__ == '__main__':
   print __doc__.strip()
