#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
roulette.py - Phenny module to dvigat' po masti.
Copyright 2014, Michael M. Vip, 0chan.hk
Licensed under the Eiffel Forum License 2.

http://inamidst.com/phenny/
"""

import re, random

vorovach = ["петух", "смотрящий", "опущеный", "стремящийся", "чухан", "малолетка", "шестерка", "блатной", "вор", "король чуханов"]
nge = ["генда", "типичный синдзи", "рей", "Аска Сорью Лэнгли", "мисато", "рицука", "рёдзи", "пен-пен", "тодзи"]
hardcore = ["трюкач ебаный", "акаб", "уроженец мытищ", "бон", "молодой хардкорщик", "ботаник", "антифа-позер", "пионер", "ОЧОБА", "наци-петух"]
madoka = ["хомура", "мадока", "саяка", "хитоми", "мами", "Кёко Сакура", "кьюбэй", "кадзуко"]

roulettes = {"vorovach": vorovach, "nge": nge, "hardcore": hardcore, "mdk": madoka}

def roll(phenny, input):
   """.roll <roulette> - Try your luck."""
   roulette = input.group(2)
   roulette = (roulette or 'vorovach').encode('utf-8')

   if roulettes.has_key(roulette):
      return phenny.reply(random.choice(roulettes[roulette]))
   else:
      return phenny.reply("опущеный")

roll.commands = ['roll']

if __name__ == '__main__': 
   print __doc__.strip()
