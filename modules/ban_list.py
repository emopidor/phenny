#!/usr/bin/env python
# coding=utf-8
"""
ban_list.py - reloadable ban list for Phenny
Copyright 2014, Akiyama Mio, 0chan.hk
Licensed under the GPLv2.

http://inamidst.com/phenny/
"""

import re

ban_list = [
# Bydlo.
"(?i).*bydlo.*!.*",
"(?i).*bylka.*!.*",
".*!d@.*",
".*!.*@.*84351D8",
".*!.*@.*6BA561CA"
]

def setup(phenny):
    phenny.config.ban_list = map(re.compile, ban_list)

if __name__ == '__main__':
   print __doc__.strip()
